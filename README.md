This is the data volume container for the [Atlassian
Bitbucket Server](https://registry.hub.docker.com/r/atlassian/bitbucket-server/) Docker image.
